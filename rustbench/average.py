import numpy
from numpy import average

def format_float(num):
    return numpy.format_float_positional(num, trim='-')

times = []
file = open("test.results", "r")
for line in file:
    linelength = len(line)
    #print(f'{linelength} chars, {line}', end='')
    if "µ" in line:
        micro_place = line.find('µ')
        number = float(line[0:micro_place])
        number = number*0.000001
        times.append(number)
        #print(f'{number:.9f}')
        continue
    if "ms" in line:
        m_place = line.find('m')
        number = float(line[0:m_place])
        number = number*0.001
        times.append(number)
        #print(f'{number:.9f}')
        continue
    if "s" in line and "ms" not in line:
        s_place = line.find('s')
        number = float(line[0:s_place])
        times.append(number)
        continue
    else:
        print('unknown input format')
print(format_float(average(times)))
