#!/bin/bash
# $1 - file name
# this script copies a template uni note .tex file to the working directory, changes it's name to $1, compiles and opens it.

var=$1                                                        # copies the name of the file to a new variable.
dirname=$(pwd | awk -F "/" '{print $NF}')                     # gets the name of the class name (current dir).
mkdir $1 && cd $1                                             # creates a directory for the lecture and cds into it.
cp $HOME/Documents/LaTeX/Templates/note_template.tex $(pwd)		# copies the template to pwd.
mv note_template.tex $1.tex						                        # renames the template file to the desired name.
var=$(echo $var | sed "s/-/ /g")                              # replaces all the "-" with spaces.
sed -i -e "s/Tytuł/$var/" $1.tex                              # replaces the title in the TeX file with the spaced version of the title.
dirname=$(echo $dirname | sed "s/-/ /g")                      # replaces all the "-" with spaces.
sed -i -e "s/Przedmiot/$dirname/" $1.tex                      # replaces the placeholder class name with the actual one.
vscodium $1.tex								                                    # opens the tex file for editing.
pdflatex -interaction=nonstopmode $1.tex >/dev/null			      # compiles the pdf and supresses the output.
zathura $1.pdf&                                                # opens the pdf without blocking the terminal.

